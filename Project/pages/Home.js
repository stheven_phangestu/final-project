import React from "react";

import {
  Alert,
  Button,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native";
import { RectButton, ScrollView } from "react-native-gesture-handler";
import Banner from "../Images/asset/banner.png";
import IcHome from "../Images/asset/iconhome.png";
import IcGambar1 from "../Images/asset/Group15.png";
import IcGambar2 from "../Images/asset/Group16.png";
import Icgambar3 from "../Images/asset/Group14.png";
import IcGambar4 from "../Images/asset/Group17.png";
import IcSaldo from "../Images/asset/Group13.png";

import { color } from "react-native-reanimated";
import IcInbox from "../Images/asset/Group10.png";
import Menu from "./Menu";

import Inbox from "./Inbox";

const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Image source={Banner} />
        <View style={{ marginBottom: 10 }}>
          <Image source={IcSaldo} style={{ width: 414, height: 108 }} />
        </View>
        <View stylee={styles.content}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              padding: 2,
              marginLeft: 2,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              Best Seller
            </Text>
            <Text
              style={{
                fontSize: 20,
                color: "yellow",
              }}
            >
              See More
            </Text>
          </View>
          {/* <View
            // style={{
            //   flexWrap: "wrap",
            }}
          > */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
              <Image
                style={{
                  width: 201,
                  height: 162,
                  resizeMode: "contain",
                  justifyContent: "center",
                }}
                source={IcGambar1}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
              <Image
                style={{
                  width: 201,
                  height: 162,
                  resizeMode: "contain",
                  justifyContent: "center",
                }}
                source={IcGambar2}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
              <Image
                style={{
                  width: 201,
                  height: 162,
                  resizeMode: "contain",
                  justifyContent: "center",
                }}
                source={Icgambar3}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
              <Image
                style={{
                  width: 201,
                  height: 162,
                  resizeMode: "contain",
                  justifyContent: "center",
                }}
                source={IcGambar4}
              />
            </TouchableOpacity>
          </View>
        </View>
        {/* </View> */}
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  content: {
    padding: 16,
    marginBottom: 10,
  },
});
