import React from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

export default function Login({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.logo}>
        <Image
          style={styles.image}
          source={require("../Images/asset/logo.png")}
        />
      </View>
      <View style={styles.inputcontainer}>
        <Text style={styles.title}>DAFTAR</Text>
        <View style={{ flex: 5, justifyContent: "space-evenly" }}>
          <TextInput placeholder="username" style={styles.input} />
          <TextInput placeholder="email" style={styles.input} />
          <TextInput placeholder="password" style={styles.input} />
          <TextInput placeholder="konfirmasi password" style={styles.input} />
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Login")}
          style={styles.button}
        >
          <Text style={styles.text}>DAFTAR</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffefd9",
    justifyContent: "center",
  },
  logo: {
    flex: 1,
    // backgroundColor:'red',
  },
  inputcontainer: {
    flex: 2,
    // backgroundColor:'grey',
    alignSelf: "center",
  },
  buttonContainer: {
    flex: 0.5,
    // backgroundColor:'orange',
    justifyContent: "space-evenly",
  },
  image: {
    height: "100%",
    resizeMode: "contain",
    alignSelf: "center",
  },
  input: {
    width: 275,
    height: 37,
    backgroundColor: "#C4C4C4",
    padding: 5,
    textAlign: "center",
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: "#7FFFD4",
    width: 275,
    height: 72,
    borderRadius: 15,
  },
  text: {
    alignSelf: "center",
    fontSize: 30,
    fontWeight: "bold",
  },
  title: {
    flex: 1,
    alignSelf: "center",
    fontSize: 35,
    fontWeight: "bold",
  },
});
