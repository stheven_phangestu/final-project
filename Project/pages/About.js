import React from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function About({ navigation, onPress }) {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="Auto" />
      <TouchableOpacity onPress={() => navigation.replace("Home")}>
        <Ionicons
          name="arrow-back"
          size={24}
          color="black"
          style={{ justifyContent: "flex-start", padding: 10 }}
        />
      </TouchableOpacity>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          source={require("../Images/asset/logo.png")}
          style={{ width: 200, height: 200 }}
        />
      </View>
      <Text style={styles.title}>About</Text>
      <View style={styles.content}>
        <View
          style={{ borderWidth: 1, paddingHorizontal: 20, paddingVertical: 30 }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginBottom: 20,
            }}
          >
            <Ionicons name="logo-whatsapp" size={30} color="black" />
            <Text>081807825483</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginBottom: 20,
            }}
          >
            <Ionicons name="logo-instagram" size={30} color="black" />
            <Text>@Stheven_phangestu</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginBottom: 20,
            }}
          >
            <MaterialCommunityIcons name="store" size={30} color="black" />
            <Text>Jl.Bangun Nusa Blok A2 no 15</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffefd9",
    justifyContent: "center",
  },
  logo: {
    flex: 1,
    width: 352,
    height: 318,
    alignSelf: "center",
    alignContent: "center",
    borderWidth: 1,
  },
  title: {
    alignSelf: "center",
    fontSize: 45,
    fontWeight: "bold",
    height: 53,
    alignContent: "center",
    borderWidth: 1,
  },
  content: {
    flex: 1,
    padding: 16,
    justifyContent: "center",
  },
});
