import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import Data from "../Images/dummy";
import { Ionicons } from "@expo/vector-icons";

const Detail = ({ navigation }) => {
  useEffect(() => {
    console.log(Data);
  }, []);

  return (
    <View style={{ backgroundColor: "#ffefd9", height: "100%" }}>
      {/* <TouchableOpacity onPress={() => navigation.replace("Home")}>
        <Ionicons
          name="arrow-back"
          size={24}
          color="black"
          style={{ justifyContent: "flex-start", padding: 10 }}
        />
      </TouchableOpacity> */}
      <Image
        source={require("../Images/asset/kopi2.png")}
        style={{
          width: 419,
          height: 251,
          justifyContent: "center",
          flexDirection: "row",
        }}
      />
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "white",
          marginBottom: 15,

          //   justifyContent: "space-between",
        }}
      >
        <Text
          style={{
            color: "black",
            fontSize: 15,

            flex: 2,
          }}
        >
          Kopi Gantung Jiwa Susu,Gula Aren,Arabica Beans Coffee Original
        </Text>
        <View
          style={{
            flex: 1,

            alignItems: "flex-end",
          }}
        >
          <Text
            style={{
              color: "black",
              fontWeight: "600",
            }}
          >
            RP. 15.000
          </Text>
        </View>
      </View>
      <View style={{ marginHorizontal: 8 }}>
        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              fontSize: 15,
              borderRadius: 10,
              flex: 1,
            }}
          >
            Suhu
          </Text>
          <View
            style={{
              justifyContent: "space-evenly",
              flexDirection: "row",
              flex: 3,
            }}
          >
            <Text
              style={{
                backgroundColor: "white",
                fontSize: 15,
                borderWidth: 1,
                borderRadius: 10,
                borderColor: "red",
              }}
            >
              Hot
            </Text>
            <Text
              style={{
                backgroundColor: "white",
                fontSize: 15,
                borderRadius: 10,
                borderColor: "blue",
                borderWidth: 1,
              }}
            >
              Cold
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", backgroundColor: "#ffefd9" }}>
          <Text
            style={{
              fontSize: 15,
              borderRadius: 10,
            }}
          >
            Size
          </Text>
          <View
            style={{
              flexDirection: "row",
              flex: 3,
            }}
          >
            <Text
              style={{
                marginLeft: 150,
                fontSize: 15,
                borderRadius: 10,
              }}
            >
              Regular
            </Text>
            <Text
              style={{
                marginLeft: 65,
                fontSize: 15,
                borderRadius: 10,
              }}
            >
              Large
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", backgroundColor: "#ffefd9" }}>
          <Text
            style={{
              fontSize: 15,
              borderRadius: 10,
              flex: 1,
            }}
          >
            Sugar
          </Text>
          <View
            style={{
              flexDirection: "row",
              flex: 3,
            }}
          >
            <Text
              style={{
                flex: 1,
                fontSize: 15,
                borderRadius: 10,
              }}
            >
              Normal
            </Text>
            <Text
              style={{
                flex: 1,
                fontSize: 15,
                borderRadius: 10,
              }}
            >
              LESS
            </Text>
            <Text
              style={{
                flex: 1,
                fontSize: 15,
                borderRadius: 10,
              }}
            >
              No Sugar
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", backgroundColor: "#ffefd9" }}>
          <Text
            style={{
              fontSize: 15,
              borderRadius: 10,
              flex: 1,
            }}
          >
            Ice
          </Text>
          <View
            style={{
              flexDirection: "row",
              flex: 3,
            }}
          >
            <Text
              style={{
                fontSize: 15,
                borderRadius: 10,
                flex: 1,
              }}
            >
              Normal
            </Text>
            <Text
              style={{
                fontSize: 15,
                borderRadius: 10,
                flex: 1,
              }}
            >
              70%
            </Text>
            <Text
              style={{
                fontSize: 15,
                borderRadius: 10,
                flex: 1,
              }}
            >
              No Ice
            </Text>
          </View>
        </View>
      </View>
      <View style={{ backgroundColor: "#ffefd9" }}>
        <Text style={{ borderWidth: 1, width: 56 }}>Notes</Text>
      </View>
      <TextInput
        placeholder=""
        style={{
          backgroundColor: "grey",
          paddingVertical: 20,
          paddingHorizontal: 20,
          padding: 15,
          height: 130,
        }}
      />
      <View style={{ flexDirection: "row", justifyContent: "center" }}>
        <TouchableOpacity
          onPress={() => navigation.navigate("App")}
          style={{
            backgroundColor: "#7FFFD4",
            justifyContent: "center",
            margin: 20,
            width: 164,
            height: 56,
            alignItems: "center",
          }}
        >
          <Text>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => alert("Barang sudah masuk")}
          style={{
            backgroundColor: "#FB0404",
            justifyContent: "center",
            margin: 20,
            width: 164,
            height: 56,
            alignItems: "center",
          }}
        >
          <Text>Beli Sekarang</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({});
