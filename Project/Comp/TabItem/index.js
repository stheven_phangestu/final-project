// import React from "react";
// import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
// import { IcHome, IcInbox, IcMenu, IcProfile } from "../../Images/asset/Index";

// const TabItem = ({ title, active, onPress, onLongPress }) => {
//   const Icon = ({ title }) => {
//     if (title === "Home") {
//       return <IcHome />;
//     }
//     if (title === "Menu") {
//       return <IcMenu />;
//     }
//     if (title === "Inbox") {
//       return <IcInbox />;
//     }
//     if (title === "Profile") {
//       return <IcProfile />;
//     }
//     return <IcHome />;
//   };

//   return (
//     <TouchableOpacity
//       style={styles.container}
//       onPress={onPress}
//       onLongPress={onLongPress}
//     >
//       <Icon />
//       <Text style={styles.text(active)}>{title}</Text>
//     </TouchableOpacity>
//   );
// };
// export default TabItem;

// const styles = StyleSheet.create({
//   container: {
//     alignItems: "center",
//   },
//   text: (active) => ({
//     fontSize: 15,
//     color: active ? "black" : "grey",
//     fontWeight: "600",
//   }),
// });
