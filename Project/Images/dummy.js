const Data = [
  {
    id: "1",
    image: require("../Images/kopi.jpeg"),
    nama: "Kopi Gantung Jiwa",
    message: "Kopi Gantung Jiwa Susu,Gula Aren,Arabica Beans Coffee Original",
    Price: "Rp. 15.000",
  },
  {
    id: "2",
    image: require("../Images/kopialpukat.jpg"),
    nama: "Kopi Gantung Jiwa Alvocado",
    message:
      "Kopi Gantung Jiwa Susu,Gula Aren,Arabica Beans Coffee Original dan alpukat",
    Price: "Rp. 20.000",
  },
  {
    id: "3",
    image: require("../Images/coklat.jpg"),
    nama: "Coklat Gantung",
    message: "coklat asli",
    Price: "Rp. 15.000",
  },
  {
    id: "4",
    image: require("../Images/coklatalpukat.jpg"),
    nama: "Coklat Alpukat Gantung",
    message: "Coklat dan alpukat",
    Price: "Rp. 25.000",
  },
];

export { Data };
