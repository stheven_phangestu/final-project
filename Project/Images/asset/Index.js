import Logo from "./logo.png";
import banner from "./banner.png";
import IcHome from "./iconhome.png";
import IcMenu from "./Group9.png";
import IcInbox from "./Group10.png";
import IcProfile from "./Group11.png";
import IcSaldo from "./Group13.png";
import Icgambar3 from "./Group14.png";
import IcGambar1 from "./Group15.png";
import IcGambar2 from "./Group16.png";
import IcGambar4 from "./Group17.png";
import Bottom from "./Bottom.png";
import IcSlide from "./Group19.png";
import backgroundsplash from "./backgroundsplash.png";

export {
  Logo,
  banner,
  IcHome,
  IcMenu,
  IcInbox,
  IcProfile,
  IcSaldo,
  Icgambar3,
  IcGambar1,
  IcGambar2,
  IcGambar4,
  Bottom,
  IcSlide,
  backgroundsplash,
};
